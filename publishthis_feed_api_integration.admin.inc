<?php

function publishthis_feed_api_integration_admin_settings() {
  $form = array();
  $form['publish_this_server'] = array(
    '#title' => t('Publish This Server'), 
    '#type' => 'textfield', 
    '#default_value' => variable_get('publish_this_server', ''), 
    '#description' => t('The publish this api server'),
  );
  $form['publish_this_key'] = array(
    '#title' => t('Publish This Key'), 
    '#type' => 'textfield', 
    '#default_value' => variable_get('publish_this_key', ''), 
    '#description' => t('The publish this key'),
  );

  $form['publish_this_expiration'] = array(
    '#title' => t('Article Expiration in Days'), 
    '#type' => 'textfield', 
    '#default_value' => variable_get('publish_this_expiration', ''), 
    '#description' => t('Imported Publish This News Items will be deleted after the indicated number of days'),
  );

  $form['publish_this_teaser_length'] = array(
    '#title' => t('Character Length of Teaser'), 
    '#type' => 'textfield', 
    '#default_value' => variable_get('publish_this_teaser_length', '200'), 
    '#description' => t('Character Length of Article Teaser Summary'),
  );

  return system_settings_form($form);
}




?>
